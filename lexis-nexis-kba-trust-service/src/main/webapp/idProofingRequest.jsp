<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head/>
   <h1 style="background-color:blue;color:white">Trust Graph Application</h1>
   <hr>
   <body style="background-color:#F5FFFA">
      <p><p>Please fill in the following details to initiate LexisNexis IdentityProofing:
      <form action="/lexisnexis/LexisnexisSurvey" method="post">
         <p>
         <input type="text" id="first" name="first">First Name<br>
         <input type="text" id="last" name="last">Last Name<br>
         <input type="text" id="year" name="year">Date of Birth - Year YYYY<br>
         <input type="text" id="month" name="month">Date of Birth - Month MM<br>
         <input type="text" id="day" name="day">Date of Birth - Day DD<br>
         <input type="text" id="addr1" name="addr1">Address Line 1<br>
         <input type="text" id="city" name="city">City<br>
         <input type="text" id="state" name="state">State<br>
         <input type="text" id="zip5" name="zip5">5-Digit Zip<br>
         <input type="hidden" id="token" name="token">
         <p>

         <!-- strip the token given from the URL and stuff it into the hidden parameter -->
         <script type="text/javascript">
           var tokenmatch = /token=(.*)/.exec(document.URL);
           document.getElementById('token').value = tokenmatch[1];
         </script>
         <input type="submit" class="LexisnexisSurvey" value="Continue">
      </form>
   </body>
</html>
