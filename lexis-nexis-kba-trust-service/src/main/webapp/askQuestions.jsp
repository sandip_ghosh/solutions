<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
   
<%@ page import="javax.servlet.http.*" %>
<%@ page import="com.resilient.ts.kba.lexisnexis.LnQuestion" %>

<%
	LnQuestion[] LnQuestions = (LnQuestion[])request.getSession().getAttribute("LnQuestions");
	StringBuilder outBuilder = new StringBuilder();
	for (int i = 0; i < LnQuestions.length; i++) {
		if (LnQuestions[i].getQuestionText() != null && !LnQuestions[i].getQuestionText().isEmpty()) {
			outBuilder.append("<p>").append(LnQuestions[i].getQuestionText()) .append("</p>\n");
			for (int j = 0; j < LnQuestions[i].getAnswerId().length; j++) {
				String choiceId = "q" + (i + 1);
				outBuilder.append("<input type='radio' id='").append(choiceId).append("' name='").append(choiceId).append("' value='").append(LnQuestions[i].getAnswerId()[j]).append("'/>").append(LnQuestions[i].getAnswerText()[j]).append("<br/>\n");
			}
		}       		
	}
	String outText = outBuilder.toString();
%>

<html>
   <h1 style="background-color:blue;color:white">Trust Graph Application</h1>
   <hr>
   <body style="background-color:#F5FFFA">
     <p>Please Answer the Following Questions:</p>
      <form action="/lexisnexis/LexisnexisProcessSurvey" method="post">
        <%=outText%>
         <p><input type="submit" class="LexisnexisProcessSurvey" value="Continue">
      </form>
   </body>
</html>
