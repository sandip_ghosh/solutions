package com.resilient.ts.kba.lexisnexis;

/**
* class representing an LN Survey Question.
**/
       public class LnQuestion {
          String quizStatus;
          String transactionId;
          String quizId;
          String lexId;
          static int numAnswerChoices = 5; // hardcoded

          String questionId;
          String questionText;
          String[] answerId = new String[numAnswerChoices];
          String[] answerText = new String[numAnswerChoices];

          public LnQuestion() {
            quizStatus = "";
            transactionId = "";
            quizId = "";
            lexId = "";
            questionId = "";
            questionText = "";
            for (int i=0; i<numAnswerChoices; i++) {
               answerId[i] = "";
            }
          }

          public String getQuizStatus() { return quizStatus; }
          public void   setQuizStatus(String a) { quizStatus = a; }

          public String getTransactionId() { return transactionId; }
          public void   setTransactionId(String a) { transactionId = a; }

          public String getQuizId() { return quizId; }
          public void   setQuizId(String a) { quizId = a; }
 
          public String getLexId() { return lexId; }
          public void   setLexId(String a) { lexId = a; }

          public String getQuestionId() { return questionId; }
          public void   setQuestionId(String a) { questionId = a; }

          public String getQuestionText() { return questionText; }
          public void   setQuestionText(String a) { questionText = a; }

          public String[] getAnswerId() { 
             //String[] b = new System.arraycopy(
             return answerId;
          }

          public void   setAnswerId(String[] a) { answerId = a; }

          public String[] getAnswerText() { return answerText; }
          public void   setAnswerText(String[] a) { answerText = a; }

     /*     public void   resetQuestions() {  // Clear the questions and answers from the object, so it can be reused in the same auth.
             // Note that the transactionId and lexId will remain the same, to retain continuity...
             quizStatus = "";
             quizId = "";
             questionId = "";
             questionText = "";
             for (i=0; i<numAnswerChoices; i++) {
                answerId[i] = "";
             }
          }
     */

}  // end class lnQuestion


