package com.resilient.ts.kba.lexisnexis;

public class KBAAuthException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public KBAAuthException(String message) {
		super(message);
	}
	
	public KBAAuthException() {
		super();
	}
}
