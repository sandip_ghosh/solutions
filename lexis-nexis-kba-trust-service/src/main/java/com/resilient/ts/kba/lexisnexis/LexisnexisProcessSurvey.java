/* ---------------------------------------------------------------------------------------------------
/ Class LexisnexisProcessSurvey
/
/ This servlet is handed the reins from LexisnexisSurvey.class.  It is called from askQuestions.jsp,
/ and handles the repeated processing of survey questions.  Quiz details are stored in the session
/ variable.  The questions are displayed by the jsp page, and answers are processed in this servlet.
/ When LN responds by asking more questions, this servlet calls askQuestuions.jsp in a circular fashion,
/ and should theoretically be able to handle multiple survey rounds.
/
/ Steve Vong     2013-10-08   Created
----------------------------------------------------------------------------------------------------*/

package com.resilient.ts.kba.lexisnexis;

import java.io.ByteArrayOutputStream;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPHeaderElement;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;



//public class LexisnexisProcessSurvey extends HttpServlet {
public class LexisnexisProcessSurvey {

	int numAnswerChoices = 5;  // hard-coded for this flow
    String username;
    String password;
	
	  public LexisnexisProcessSurvey(String username, String password) {
		  this.username = username;
		  this.password = password;
	  }
	  
      /**
       * Call the LN IdentityProofing service, and get a response
       **/
       private SOAPMessage LNIdentityProofing(SOAPMessage SOAPRequestMessage, String weburl) throws Exception
       {
           // Create SOAP Connection
           SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
           SOAPConnection soapConnection = soapConnectionFactory.createConnection();
        
           // Make call to the lexisnexis IdentityProofing service
           SOAPMessage soapResponse = soapConnection.call(SOAPRequestMessage, weburl);

           // close the connection
           soapConnection.close();   

           return soapResponse;
       }



      /**
       * Function to construct the LexisNexis API Survey SOAP envelope
       **/
       private SOAPMessage constructSurveyResponse(String txId, String custRefId, String quizId, String question1, String answer1, String question2, String answer2, String question3, String answer3) throws Exception
       {
          String WSSE_NS = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
          String WSU_NS = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";
          String WSSE_SECURITY_LNAME = "Security";
          String WSSE_NS_PREFIX = "wsse";
          String wsNamespace = "http://ws.identityproofing.idm.risk.lexisnexis.com/";
          String nsNamespace = "http://ns.lexisnexis.com/identity-proofing/1.0";
          String ns1Namespace = "http://ns.lexisnexis.com/survey/1.0";
//          String username = "10832/PCC"; //login to LexisNexis Staging
//          String password = "RNSg@teway1";
//          String password = "rnsgateway!1";
          String workflow = "RESILIENTPCC_FLOW";

          // Instantiate a soapmessage instance  and grab the soap part to add element to
          SOAPFactory sf = SOAPFactory.newInstance();
          MessageFactory messageFactory = MessageFactory.newInstance();
          SOAPMessage soapMessage = messageFactory.createMessage();
          SOAPPart soapPart = soapMessage.getSOAPPart();

          // Get the soap envelope section from the xml and add a name space to it
          SOAPEnvelope envelope = soapPart.getEnvelope();
          envelope.addNamespaceDeclaration("ws", wsNamespace);
          envelope.addNamespaceDeclaration("ns", nsNamespace);
          envelope.addNamespaceDeclaration("ns1", ns1Namespace);

          // Construct the soap body of the request
          SOAPBody soapBody = envelope.getBody();
          SOAPElement soapBodyElem = soapBody.addChildElement("invokeIdentityService", "ws");
          SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("identityProofingRequest", "ns");
            Name transactionID = sf.createName("transactionID", "ns", "");
             soapBodyElem1.addAttribute(transactionID, txId);
/*
            Name locale = sf.createName("locale", "ns", "");
             soapBodyElem1.addAttribute(locale, req_locale);
            Name custref = sf.createName("customerReference", "ns", "");
             soapBodyElem1.addAttribute(custref, custRefId);
*/
          SOAPElement soapBodyElem2 = soapBodyElem1.addChildElement("workFlow", "ns");
            soapBodyElem2.addTextNode(workflow);
          SOAPElement soapBodyElem3 = soapBodyElem1.addChildElement("scoreRequest", "ns");
          SOAPElement soapBodyElem4 = soapBodyElem3.addChildElement("quizId", "ns");
            soapBodyElem4.addTextNode(quizId);

          SOAPElement soapBodyElem5 = soapBodyElem3.addChildElement("answer", "ns");
          SOAPElement soapBodyElem6 = soapBodyElem5.addChildElement("questionId", "ns");
            soapBodyElem6.addTextNode(question1);
          SOAPElement soapBodyElem7 = soapBodyElem5.addChildElement("choiceId", "ns");
            soapBodyElem7.addTextNode(answer1);

          // There may be 1 or 3 questions
          if ((question2 == null) || (!question2.equals(""))) {
             SOAPElement soapBodyElem8 = soapBodyElem3.addChildElement("answer", "ns");
             SOAPElement soapBodyElem9 = soapBodyElem8.addChildElement("questionId", "ns");
               soapBodyElem9.addTextNode(question2);
             SOAPElement soapBodyElem10 = soapBodyElem8.addChildElement("choiceId", "ns");
               soapBodyElem10.addTextNode(answer2);
          }

          if ((question3 == null) || (!question3.equals(""))) {
             SOAPElement soapBodyElem11 = soapBodyElem3.addChildElement("answer", "ns");
             SOAPElement soapBodyElem12 = soapBodyElem11.addChildElement("questionId", "ns");
               soapBodyElem12.addTextNode(question3);
             SOAPElement soapBodyElem13 = soapBodyElem11.addChildElement("choiceId", "ns");
               soapBodyElem13.addTextNode(answer3);
          }


          //Create the xml Security header
          SOAPHeader header = envelope.getHeader();

          Name securityName = sf.createName(WSSE_SECURITY_LNAME, WSSE_NS_PREFIX, WSSE_NS);
          SOAPHeaderElement securityElem = header.addHeaderElement(securityName);
          securityElem.setMustUnderstand(true);

          Name usernameTokenName = sf.createName("UsernameToken", WSSE_NS_PREFIX, WSSE_NS);
          SOAPElement usernameTokenMsgElem = sf.createElement(usernameTokenName);
            Name wsuid = sf.createName("Id", "wsu", WSU_NS);
             usernameTokenMsgElem.addAttribute(wsuid, "UsernameToken-49");

          Name usernameName = sf.createName("Username", WSSE_NS_PREFIX, WSSE_NS);
          SOAPElement usernameMsgElem = sf.createElement(usernameName);
          usernameMsgElem.addTextNode(username);
          usernameTokenMsgElem.addChildElement(usernameMsgElem);

          Name passwordName = sf.createName("Type", WSSE_NS_PREFIX, WSSE_NS);
          SOAPElement passwordMsgElem = sf.createElement("Password", WSSE_NS_PREFIX, WSSE_NS);

          //passwordMsgElem.addAttribute(passwordName, PASSWORD_TEXT_TYPE);
          passwordMsgElem.addTextNode(password);
          usernameTokenMsgElem.addChildElement(passwordMsgElem);

          securityElem.addChildElement(usernameTokenMsgElem);


          soapMessage.saveChanges();

          return soapMessage;
      }
    


      /**
       * Method used to process SOAP response from LN
       **/
       private LnQuestion[] processIdentityProofingResponse(SOAPMessage soapResponse)throws Exception{
           //Use the Document builder functionality to parse xml by elements.
           String result = null;
           ByteArrayOutputStream baos = null;
            
           baos = new ByteArrayOutputStream();
           soapResponse.writeTo(baos); 
           result = baos.toString();
 
           DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
           DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
           InputSource is = new InputSource(new StringReader(result));
           Document doc = dBuilder.parse(is);

           XPathFactory xPathfactory = XPathFactory.newInstance();
           XPath xpath = xPathfactory.newXPath();
           //System.out.println("--- SECOND SERVLET ---" + result);  // Debugging

           LnQuestion[] theQuestions;

         try { 

           //-----------------------------------------------
           // Use XPath to extract the verification results
           //-----------------------------------------------
           String status    = xpath.compile("//productResponse[@type='ns:InstantAuthenticateResponse']/status/text()").evaluate(doc);

           // If auth passes, just basically pass back the OK in the question array.
           // If auth is Pending, then we have to construct the question array with the new questions.
           // If auth fails, pass back the failure in the question array.

           if (status.equals("PASS")) {
               theQuestions =  new LnQuestion[1];  // first, reserve the array size
               theQuestions[0] = new LnQuestion();  // then, have to initialize the array member!
               theQuestions[0].setQuizStatus(status);
                // note, this quizId refers to the PREVIOUS quiz.
               theQuestions[0].setQuizId(xpath.compile("//productResponse[@type='ns:InstantAuthenticateResponse']/quizScore/@quizId").evaluate(doc));
               theQuestions[0].setLexId(xpath.compile("//productResponse[@type='ns:InstantAuthenticateResponse']/@LexID").evaluate(doc));
               theQuestions[0].setTransactionId(xpath.compile("//identityProofingResponse/@transactionID").evaluate(doc));
               return theQuestions;

           } else if (status.equals("PENDING")) {
               int numQuestions = Integer.parseInt(xpath.compile("//productResponse[@type='ns:InstantAuthenticateResponse']/questions/numQuestions/text()").evaluate(doc));

               theQuestions =  new LnQuestion[numQuestions];

               String quizId = xpath.compile("//productResponse[@type='ns:InstantAuthenticateResponse']/questions/@quizId").evaluate(doc); // this is the NEW quiz's ID
               String lexisnexisId = xpath.compile("//productResponse[@type='ns:InstantAuthenticateResponse']/@LexID").evaluate(doc);
               String transId = xpath.compile("//identityProofingResponse/@transactionID").evaluate(doc);


               //------------------------------------------------------------------------
               // loop through the questions. There may be 1-3 of them; 5 choices each.
               //------------------------------------------------------------------------
               for (int i=1; i<=numQuestions; i++) {
                  theQuestions[i-1] = new LnQuestion();  // initialize the array member first

                  // Fill in the quiz identifiers
                  theQuestions[i-1].setQuizStatus(status);
                  theQuestions[i-1].setQuizId(quizId);
                  theQuestions[i-1].setTransactionId(transId);
                  theQuestions[i-1].setLexId(lexisnexisId);

                  // Question ID and Text; 1 per question.
                  theQuestions[i-1].setQuestionId(xpath.compile("//productResponse[@type='ns:InstantAuthenticateResponse']/questions/question[@sequenceId='" + 
                                    String.valueOf(i) + "']/@questionId").evaluate(doc));

                  theQuestions[i-1].setQuestionText(xpath.compile("//productResponse[@type='ns:InstantAuthenticateResponse']/questions/question[@sequenceId='" + 
                                    String.valueOf(i) + "']/text/text()").evaluate(doc));


                  // Question choices; populate the array with the 5 provided answer choices
//!!!!!!!!!!!!!!!!!!! shouldn't be directly modifying class members
                  for (int j=1; j<=numAnswerChoices; j++) {
                     //theQuestions[i-1].answerId[j-1] = new answerId();  // Initialize the array member

                     theQuestions[i-1].answerId[j-1] = xpath.compile("//productResponse[@type='ns:InstantAuthenticateResponse']/questions/question[@sequenceId='" + 
                                    String.valueOf(i) + "']/choice[@sequenceId='" + String.valueOf(j) + "']/@choiceId").evaluate(doc);

                     theQuestions[i-1].answerText[j-1] = xpath.compile("//productResponse[@type='ns:InstantAuthenticateResponse']/questions/question[@sequenceId='" + 
                                    String.valueOf(i) +"']/choice[@sequenceId='" + String.valueOf(j) + "']/text/text()").evaluate(doc);
                  }  // answer choices loop
               }  // questions loop

               return theQuestions;

           } else {  // verification FAILS for ANY other response

               theQuestions =  new LnQuestion[1];
               theQuestions[0] = new LnQuestion();
               theQuestions[0].setQuizStatus(status);
               theQuestions[0].setQuizId(xpath.compile("//productResponse[@type='ns:InstantAuthenticateResponse']/quizScore/@quizId").evaluate(doc));
               theQuestions[0].setLexId(xpath.compile("//productResponse[@type='ns:InstantAuthenticateResponse']/@LexID").evaluate(doc));
               theQuestions[0].setTransactionId(xpath.compile("//identityProofingResponse/@transactionID").evaluate(doc));

               return theQuestions;
           }
         } catch (Exception e) {
             System.err.println("Error occurred processing IdentityProofing response");
             e.printStackTrace();
         } // try

           // Any other way to do this without getting a return error?
           theQuestions = new LnQuestion[1];
           return theQuestions;
        }


       public LnQuestion[] verifyIdentityAnswers(String quizId, String txId, String q1Id, String a1, String q2Id, String a2, String q3Id, String a3) throws KBAAuthException {
    	   try {
               String url = "https://staging.identitymanagement.lexisnexis.com/identity-proofing/services/identityProofingServiceWS/v2";
               SOAPMessage soaprequest = constructSurveyResponse(txId,"1234",quizId, q1Id, a1, q2Id, a2, q3Id, a3);

               SOAPMessage soapresponse = LNIdentityProofing(soaprequest, url);

               LnQuestion[] lnResponse = processIdentityProofingResponse(soapresponse);  // LN can respond with another survey
    		   return lnResponse;
    		   
    	   } catch (Exception ex) {
    		   throw new KBAAuthException(ex.getLocalizedMessage());
    	   }

       }

 }
