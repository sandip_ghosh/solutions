package com.resilient.ts.kba.lexisnexis;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

@Path("/kba/parameters")
public class KBAParametersResource {

    @GET
    @Produces("application/json")
    public Map getParameters(@HeaderParam("host") String host,
                             @HeaderParam("x-authorization-key") String key,
                             @Context HttpServletRequest request) {
        List<Map<String, String>> params = new ArrayList<Map<String, String>>(5);
        Map<String, String> param;
        param = new HashMap<String, String>(11);
        param = new HashMap<String, String>(11);
        param.put("Name", "Lexis Nexis API Username");
        param.put("Description", "The Username of the credentials that will be used to call the Lexis Nexis API");
        param.put("Type", "INPUT");
        param.put("Fixed", "true");
        params.add(param);
        param = new HashMap<String, String>(11);
        param.put("Name", "Lexis Nexis API Password");
        param.put("Description", "The Password of the credentials that will be used to call the Lexis Nexis API");
        param.put("Type", "INPUT");
        param.put("Fixed", "true");
        param.put("Masked", "true");
        params.add(param);
        param = new HashMap<String, String>(11);
        param.put("Name", "Name");
        param.put("Description", "The full name of the user requesting identity proofing");
        param.put("Type", "INPUT");
        params.add(param);
        param = new HashMap<String, String>(11);
        param.put("Name", "Date Of Birth");
        param.put("Description", "The date of birth of the user requesting identity proofing (MM/DD/YYYY)");
        param.put("Type", "INPUT");
        params.add(param);
        param = new HashMap<String, String>(11);
        param.put("Name", "Street Address");
        param.put("Description", "The street address of the user requesting identity proofing");
        param.put("Type", "INPUT");
        params.add(param);
        param = new HashMap<String, String>(11);
        param.put("Name", "City");
        param.put("Description", "The city of the user requesting identity proofing");
        param.put("Type", "INPUT");
        params.add(param);
        param = new HashMap<String, String>(11);
        param.put("Name", "State");
        param.put("Description", "The state of the user requesting identity proofing");
        param.put("Type", "INPUT");
        params.add(param);
        param = new HashMap<String, String>(11);
        param.put("Name", "Zip Code");
        param.put("Description", "The zip code of the user requesting identity proofing");
        param.put("Type", "INPUT");
        params.add(param);

        String protocol = "http";
        try {
        	URI reqURI = new URI(request.getRequestURI());
        	protocol = reqURI.getScheme() != null && !reqURI.getScheme().isEmpty() ? reqURI.getScheme() : "http";
        } catch (URISyntaxException use) {
        	protocol = "http";
        }
        Map<String, Object> retMap = new HashMap<String, Object>(11);
        retMap.put("exec_url", protocol + "://" + host + "/lexisnexis/kba/exec");
        retMap.put("params", params);
        return retMap;
    }
	
}
