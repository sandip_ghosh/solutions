/* ---------------------------------------------------------------------------------------------------
/ Class LexisnexisSurvey
/
/ Class to verify a user using the LexisNexis IdentityProofing service. Although the methods refer to
/ InstantVerify, this service actually uses InstantAuthenticate.
/
/ The idProofingRequest.jsp page calls this servlet with User name, address, and DoB.  This servlet
/ calls LN, which responds with 3 questions.  The questions are parsed and then passed on to 
/ askQuestions.jsp to be answered.
/
/ Steve Vong     2013-10-02   Created
----------------------------------------------------------------------------------------------------*/

package com.resilient.ts.kba.lexisnexis;

import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPHeaderElement;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;


//public class LexisnexisSurvey extends HttpServlet {
public class LexisnexisSurvey {
      int numAnswerChoices = 5;  // hard-coded for this flow
      String username;
      String password;
      
      LexisnexisSurvey(String username, String password) {
    	this.username = username;
    	this.password = password;
      }
      
      /**
       * Call the LN InstantVerify service, and get a response
       **/
       private SOAPMessage LNInstantVerify(SOAPMessage SOAPRequestMessage, String weburl) throws Exception
       {
           // Create SOAP Connection
           SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
           SOAPConnection soapConnection = soapConnectionFactory.createConnection();
        
           // Make call to the lexisnexis InstantVerify service
           SOAPMessage soapResponse = soapConnection.call(SOAPRequestMessage, weburl);

           // close the connection
           soapConnection.close();   

           return soapResponse;
       }



      /**
       * Function to construct the LexisNexis API Initial SOAP envelope
       * Note that no type checking is done.  All input values are set as String... 
       **/
       private SOAPMessage constructInitialInstantVerify(String firstName, String lastName, String dobYear, String dobMonth, String dobDay, String addr1, String addrCity, String addrState, String addrZip, String custRefId) throws Exception
       {
          String WSSE_NS = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
          String PASSWORD_TEXT_TYPE = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText";
          String WSU_NS = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";
          String WSSE_SECURITY_LNAME = "Security";
          String WSSE_NS_PREFIX = "wsse";
          String wsNamespace = "http://ws.identityproofing.idm.risk.lexisnexis.com/";
          String nsNamespace = "http://ns.lexisnexis.com/identity-proofing/1.0";
          Date   date = new Date();
          SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HHmmssSSS");
//          String username = "10832/PCC"; //login to LexisNexis Staging
//          String password = "rnsgateway!1";
//          String password = "RNSg@teway1";
          String workflow = "RESILIENTPCC_FLOW";

          String req_locale = "en_US";
          String addressType = "PRIMARY_RESIDENCE";

          // Instantiate a soapmessage instance  and grab the soap part to add element to
          SOAPFactory sf = SOAPFactory.newInstance();
          MessageFactory messageFactory = MessageFactory.newInstance();
          SOAPMessage soapMessage = messageFactory.createMessage();
          SOAPPart soapPart = soapMessage.getSOAPPart();

          // Get the soap envelope section from the xml and add a name space to it
          SOAPEnvelope envelope = soapPart.getEnvelope();
          envelope.addNamespaceDeclaration("ws", wsNamespace);
          envelope.addNamespaceDeclaration("ns", nsNamespace);


        try {
          // Construct the soap body of the request
          SOAPBody soapBody = envelope.getBody();
          SOAPElement soapBodyElem = soapBody.addChildElement("invokeIdentityService", "ws");
          SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("identityProofingRequest", "ns");
            Name transactionID = sf.createName("transactionID", "ns", "");
             soapBodyElem1.addAttribute(transactionID, sdf.format(date));
            Name locale = sf.createName("locale", "ns", "");
             soapBodyElem1.addAttribute(locale, req_locale);
            Name custref = sf.createName("customerReference", "ns", "");
             soapBodyElem1.addAttribute(custref, custRefId);
          SOAPElement soapBodyElem2 = soapBodyElem1.addChildElement("workFlow", "ns");
            soapBodyElem2.addTextNode(workflow);
          SOAPElement soapBodyElem3 = soapBodyElem1.addChildElement("inputSubject", "ns");
          SOAPElement soapBodyElem4 = soapBodyElem3.addChildElement("person", "ns");

          SOAPElement soapBodyElem5 = soapBodyElem4.addChildElement("dateOfBirth", "ns");
          SOAPElement soapBodyElem6 = soapBodyElem5.addChildElement("Year", "ns");
            soapBodyElem6.addTextNode(dobYear);
          SOAPElement soapBodyElem7 = soapBodyElem5.addChildElement("Month", "ns");
            soapBodyElem7.addTextNode(dobMonth);
          SOAPElement soapBodyElem8 = soapBodyElem5.addChildElement("Day", "ns");
            soapBodyElem8.addTextNode(dobDay);

          SOAPElement soapBodyElem9 = soapBodyElem4.addChildElement("name", "ns");
          SOAPElement soapBodyElem10 = soapBodyElem9.addChildElement("first", "ns");
            soapBodyElem10.addTextNode(firstName);
          SOAPElement soapBodyElem11 = soapBodyElem9.addChildElement("last", "ns");
            soapBodyElem11.addTextNode(lastName);

          SOAPElement soapBodyElem12 = soapBodyElem4.addChildElement("address", "ns");
            Name addressPurpose = sf.createName("addressPurpose", "ns", "");
             soapBodyElem1.addAttribute(addressPurpose, addressType);
          SOAPElement soapBodyElem13 = soapBodyElem12.addChildElement("addressLine1", "ns");
            soapBodyElem13.addTextNode(addr1);
          SOAPElement soapBodyElem14 = soapBodyElem12.addChildElement("city", "ns");
            soapBodyElem14.addTextNode(addrCity);
          SOAPElement soapBodyElem15 = soapBodyElem12.addChildElement("stateCode", "ns");
            soapBodyElem15.addTextNode(addrState);
          SOAPElement soapBodyElem16 = soapBodyElem12.addChildElement("zip5", "ns");
            soapBodyElem16.addTextNode(addrZip);


          //Create the xml Security header
          SOAPHeader header = envelope.getHeader();

          Name securityName = sf.createName(WSSE_SECURITY_LNAME, WSSE_NS_PREFIX, WSSE_NS);
          SOAPHeaderElement securityElem = header.addHeaderElement(securityName);
          securityElem.setMustUnderstand(true);

          Name usernameTokenName = sf.createName("UsernameToken", WSSE_NS_PREFIX, WSSE_NS);
          SOAPElement usernameTokenMsgElem = sf.createElement(usernameTokenName);
            Name wsuid = sf.createName("Id", "wsu", WSU_NS);
             usernameTokenMsgElem.addAttribute(wsuid, "UsernameToken-49");

          Name usernameName = sf.createName("Username", WSSE_NS_PREFIX, WSSE_NS);
          SOAPElement usernameMsgElem = sf.createElement(usernameName);
          usernameMsgElem.addTextNode(username);
          usernameTokenMsgElem.addChildElement(usernameMsgElem);

          Name passwordName = sf.createName("Type", WSSE_NS_PREFIX, WSSE_NS);
          SOAPElement passwordMsgElem = sf.createElement("Password", WSSE_NS_PREFIX, WSSE_NS);

          //passwordMsgElem.addAttribute(passwordName, PASSWORD_TEXT_TYPE);
          passwordMsgElem.addTextNode(password);
          usernameTokenMsgElem.addChildElement(passwordMsgElem);

          securityElem.addChildElement(usernameTokenMsgElem);
        } catch (Exception e) {
             System.err.println("Error occurred creating SOAP message");
             e.printStackTrace();
             return soapMessage;
        }


          soapMessage.saveChanges();

          /* Used for Debugging */
          /*
          System.out.print("Request SOAP Message = ");
          soapMessage.writeTo(System.out);
          System.out.println();
          */

          return soapMessage;
      }


      /**
       * Method used to process SOAP response from LN InstantVerify
       **/
       private LnQuestion[] processInstantVerifyResponse(SOAPMessage soapResponse)throws Exception{
           //Use the Document builder functionality to parse xml by elements.
           String result = null;
           ByteArrayOutputStream baos = null;
            
           baos = new ByteArrayOutputStream();
           soapResponse.writeTo(baos); 
           result = baos.toString();
 
           DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
           DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
           InputSource is = new InputSource(new StringReader(result));
           Document doc = dBuilder.parse(is);

           XPathFactory xPathfactory = XPathFactory.newInstance();
           XPath xpath = xPathfactory.newXPath();

           LnQuestion[] theQuestions;

         try { 

           //-----------------------------------------------
           // Use XPath to extract the verification results
           //-----------------------------------------------
           String status    = xpath.compile("//productResponse[@type='ns:InstantAuthenticateResponse']/status/text()").evaluate(doc);

           if (status.equals("PASS")) {
               theQuestions =  new LnQuestion[1];
               theQuestions[0].setQuizStatus(status);
               theQuestions[0].setQuizId(xpath.compile("//productResponse[@type='ns:InstantAuthenticateResponse']/quizScore/@quizId").evaluate(doc));
               theQuestions[0].setLexId(xpath.compile("//productResponse[@type='ns:InstantAuthenticateResponse']/@LexID").evaluate(doc));
               theQuestions[0].setTransactionId(xpath.compile("//identityProofingResponse/@transactionID").evaluate(doc));
               return theQuestions;

           } else if (status.equals("PENDING")) {
               int numQuestions = Integer.parseInt(xpath.compile("//productResponse[@type='ns:InstantAuthenticateResponse']/questions/numQuestions/text()").evaluate(doc));

               theQuestions =  new LnQuestion[numQuestions];

               String quizId = xpath.compile("//productResponse[@type='ns:InstantAuthenticateResponse']/questions/@quizId").evaluate(doc);
               String lexisnexisId = xpath.compile("//productResponse[@type='ns:InstantAuthenticateResponse']/@LexID").evaluate(doc);
               String transId = xpath.compile("//identityProofingResponse/@transactionID").evaluate(doc);


               //------------------------------------------------------------------------
               // loop through the questions. There may be 1-3 of them; 5 choices each.
               //------------------------------------------------------------------------
               for (int i=1; i<=numQuestions; i++) {
                  theQuestions[i-1] = new LnQuestion();  // initialize the array member first

                  // Fill in the quiz identifiers
                  theQuestions[i-1].setQuizStatus(status);
                  theQuestions[i-1].setQuizId(quizId);
                  theQuestions[i-1].setTransactionId(transId);
                  theQuestions[i-1].setLexId(lexisnexisId);

                  // Question ID and Text; 1 per question.
                  theQuestions[i-1].setQuestionId(xpath.compile("//productResponse[@type='ns:InstantAuthenticateResponse']/questions/question[@sequenceId='" + 
                                    String.valueOf(i) + "']/@questionId").evaluate(doc));

                  theQuestions[i-1].setQuestionText(xpath.compile("//productResponse[@type='ns:InstantAuthenticateResponse']/questions/question[@sequenceId='" + 
                                    String.valueOf(i) + "']/text/text()").evaluate(doc));


                  // Question choices; populate the array with the 5 provided answer choices
//!!!!!!!!!!!!!!!!!!!!!!
                  for (int j=1; j<=numAnswerChoices; j++) {
                     //theQuestions[i-1].answerId[j] = new answerId();

                     theQuestions[i-1].answerId[j-1] = xpath.compile("//productResponse[@type='ns:InstantAuthenticateResponse']/questions/question[@sequenceId='" + 
                                    String.valueOf(i) + "']/choice[@sequenceId='" + String.valueOf(j) + "']/@choiceId").evaluate(doc);

                     theQuestions[i-1].answerText[j-1] = xpath.compile("//productResponse[@type='ns:InstantAuthenticateResponse']/questions/question[@sequenceId='" + 
                                    String.valueOf(i) +"']/choice[@sequenceId='" + String.valueOf(j) + "']/text/text()").evaluate(doc);
                  }  // answer choices loop
               }  // questions loop

               return theQuestions;

           } else {  // verification FAILS

               // NEED CODE HERE TO HANDLE FAILS !!
               theQuestions =  new LnQuestion[1];
               theQuestions[0] = new LnQuestion();  //crucial
               theQuestions[0].setQuizStatus(status);
               theQuestions[0].setQuizId(xpath.compile("//productResponse[@type='ns:InstantAuthenticateResponse']/questions/@quizId").evaluate(doc));
               theQuestions[0].setLexId(xpath.compile("//productResponse[@type='ns:InstantAuthenticateResponse']/@LexID").evaluate(doc));
               theQuestions[0].setTransactionId(xpath.compile("//identityProofingResponse/@transactionID").evaluate(doc));

               return theQuestions;
           }
         } catch (Exception e) {
             System.err.println("Error occurred processing SOAP response round1");
             e.printStackTrace();
         } // try

           // Any other way to do this without getting a return error?
           theQuestions = new LnQuestion[1];
           return theQuestions;
        }


      public LnQuestion[] invokeLexisNexisAPI(String name, String dob, String addr1, String city, String state, String zip) throws KBAAuthException {
    	  if (name == null || name.isEmpty() ||
    		  dob == null || dob.isEmpty() ||
    		  addr1 == null || addr1.isEmpty() ||
    		  city == null || city.isEmpty() ||
    		  state == null || state.isEmpty() ||
    		  zip == null || zip.isEmpty()) {
    		  throw new KBAAuthException("Required fields are not set or have empty values");
    	  }
    	  String firstName, lastName, year, month, day;
    	  String nameParts[] = name.split(" ");
    	  if (nameParts.length >= 2) {
    		  firstName = nameParts[0];
    		  lastName = "";
    		  for (int i = 1; i < nameParts.length; i++) {
    			  if (!lastName.isEmpty()) {
    				  lastName += " ";
    			  }
    			  lastName += nameParts[i];
    		  }
    	  } else {
    		  throw new KBAAuthException("Name does not contain first and last name");
    	  }
    	  String[] dateParts = dob.split("/");
    	  if (dateParts.length == 3) {
    		  month = dateParts[0];
    		  day = dateParts[1];
    		  year = dateParts[2];
    	  } else {
    		  throw new KBAAuthException("Date of Birth not in correct format");
    	  }
    	  try {
	    	  String url = "https://staging.identitymanagement.lexisnexis.com/identity-proofing/services/identityProofingServiceWS/v2";
	          SOAPMessage soaprequest = constructInitialInstantVerify(firstName,lastName,year,month,day,addr1,city,state,zip,"1234");
	          //getServletContext().log("-LN InstantVerify request constructed-"); 
	
	
	          SOAPMessage soapresponse = LNInstantVerify(soaprequest, url);
	          //getServletContext().log("-LN InstantVerify request sent-"); 
	
	
	          LnQuestion[] theQuestions = processInstantVerifyResponse(soapresponse);
	          return theQuestions;
    	  } catch (Exception ex) {
    		  throw new KBAAuthException(ex.getLocalizedMessage());
    	  }
      }

      
}
