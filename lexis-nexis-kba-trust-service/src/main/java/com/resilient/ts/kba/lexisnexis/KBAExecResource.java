package com.resilient.ts.kba.lexisnexis;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

@Path("/kba/exec")
public class KBAExecResource {

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Map exec(@HeaderParam("x-authorization-key") String key, Map args) {
        Map<String, Object> result = new HashMap<String, Object>(11);
        try {
        	String lnAPIUser = (String)args.get("Lexis Nexis API Username");
        	String lnAPIPassword = (String)args.get("Lexis Nexis API Password");
        	String quizId = (String)args.get("quizId");
        	LnQuestion[] questions = null;
        	if (quizId == null) {
        		// we are making the first request to the API for getting identify proofing questions
	        	LexisnexisSurvey survey = new LexisnexisSurvey(lnAPIUser, lnAPIPassword);
	        	String name = (String)args.get("Name");
	        	String dob = (String)args.get("Date Of Birth");
	        	String addr1 = (String)args.get("Street Address");
	        	String city = (String)args.get("City");
	        	String state = (String)args.get("State");
	        	String zip = (String)args.get("Zip Code");
	        	questions = survey.invokeLexisNexisAPI(name, dob, addr1, city, state, zip);
        	} else {
        		// we have some answers to the identity proofing questions we want to verify the answers
        		String a1 = (String)args.get("q1");
        		String q1Id = (String)args.get("q1Id");
        		String a2 = (String)args.get("q2");
        		String q2Id = (String)args.get("q2Id");
        		String a3 = (String)args.get("q3");
        		String q3Id = (String)args.get("q3Id");
        		String txId = (String)args.get("transactionId");
        		LexisnexisProcessSurvey surveyResp = new LexisnexisProcessSurvey(lnAPIUser, lnAPIPassword);
        		questions = surveyResp.verifyIdentityAnswers(quizId, txId, q1Id, a1, q2Id, a2, q3Id, a3);
        	}
            if (questions != null && questions.length > 0) {
            	if (questions[0].getQuizStatus().equals("PASS")) {
	            	result.put("result", "GRANT");
	            } else if (questions[0].getQuizStatus().equals("PENDING")) {
	            	result.put("result", "TB_DISPLAY");
	            	JSONObject tbdMetaData = getTBDMetaData(questions);
	            	result.put("tbdMetaData", tbdMetaData.toString());
	            } else {
	            	result.put("result", "DENY");
	            }
            } else {
            	result.put("result", "DENY");
            }
        	return result;
        } catch (Exception e) {
            result.put("result", "ERROR");
            result.put("error message", e.getMessage());
        }
        return result;
    }
    
    private JSONObject getTBDMetaData(LnQuestion[] questions) throws JSONException {
    	JSONObject tbdMetaData = new JSONObject();
    	tbdMetaData.put("instructionText", "Please select the correct answers for the following questions to verify your identity.");
    	JSONArray itemArray = new JSONArray();
    	for (int i = 0; i < questions.length; i++) {
    		JSONObject item = new JSONObject();
    		item.put("name", "q" + (i + 1));
    		item.put("label", questions[i].getQuestionText());
    		item.put("type", "Radio");
    		JSONArray optionsArray = new JSONArray();
    		for (int j = 0; j < questions[i].getAnswerId().length; j++) {
    			JSONObject option = new JSONObject();
    			option.put("value", questions[i].getAnswerId()[j]);
    			option.put("label", questions[i].getAnswerText()[j]);
    			optionsArray.put(option);
    		}
    		item.put("options", optionsArray);
    		itemArray.put(item);
    		// create a hidden question id parameter
    		item = new JSONObject();
    		item.put("type", "Hidden");
    		item.put("name", "q" + (i + 1) + "Id");
    		item.put("value", questions[i].getQuestionId());
    		itemArray.put(item);
    	}
    	// create additional hidden items for the quiz ID and Transaction ID for the verification
    	JSONObject item = new JSONObject();
    	item.put("type", "Hidden");
    	item.put("name", "transactionId");
    	item.put("value", questions[0].getTransactionId());
    	itemArray.put(item);
    	item = new JSONObject();
    	item.put("type", "Hidden");
    	item.put("name", "quizId");
    	item.put("value", questions[0].getQuizId());
    	itemArray.put(item);
    	tbdMetaData.put("items", itemArray);
    	return tbdMetaData;
    }
}
